---
lang: fr
date: 20/03/2024
author: Lamour Enzo, Lannoy Noa, Wilfart Axel
title: README
---

## Compilation du Rapport(Pandoc)

> * **En html :**

```shell
pandoc --metadata 'mainfont=-apple-system, BlinkMacSystemFont, Segoe UI, sans-serif' --metadata monobackgroundcolor=#f0f0f0 --output /Users/enzolamour/Documents/BUT1-2/SAE203/rapport.html --to html --standalone
```

> * **En pdf :**

```shell
pandoc --output /Users/enzolamour/Documents/BUT1-2/SAE203/rapport.pdf --to latex --standalone
```